﻿#include <iostream>
#include <vector>

#define X 0
#define Y 1
#define Z 2

#define VERTICESCOUNT 4
#define FACESCOUNT 4

class Point {
public:
    double coords[3];

    Point()
    {
        for (int i = 0; i < 3; i++)
        {
            coords[i] = 0;
        }
    };
    Point(std::initializer_list<double> coords)
    {
        if (2 >= coords.size()) {
            throw std::invalid_argument("Недостаточно координат для инициализации точки");
        }

        int i = 0;
        for (double coord : coords)
        {
            this->coords[i] = coord;
            i++;
        }
    };
};

class Surface {
private:
    double A, B, C, D;
    
    void getCoefs(std::vector<Point> points)
    {
        if (nullptr == points.data()) {
            throw std::invalid_argument("Недопустимые аргументы для функции получения коэффициентов");
        }

        A = (points[1].coords[Y] - points[0].coords[Y]) * (points[2].coords[Z] - points[0].coords[Z]) -
            (points[1].coords[Z] - points[0].coords[Z]) * (points[2].coords[Y] - points[0].coords[Y]);

        B = (points[1].coords[Z] - points[0].coords[Z]) * (points[2].coords[X] - points[0].coords[X]) -
            (points[1].coords[X] - points[0].coords[X]) * (points[2].coords[Z] - points[0].coords[Z]);

        C = (points[1].coords[X] - points[0].coords[X]) * (points[2].coords[Y] - points[0].coords[Y]) -
            (points[1].coords[Y] - points[0].coords[Y]) * (points[2].coords[X] - points[0].coords[X]);

        D = (-points[0].coords[X] * A - points[0].coords[Y] * B - points[0].coords[Z] * C);
    }

public:
    //Значение при подстановки точки в уравнение плоскости
    double getValue(Point point) {
        if (nullptr == point.coords) {
            throw std::invalid_argument("Точка непроинициализирована");
        }
        return this->A * point.coords[X] + this->B * point.coords[Y] + this->C * point.coords[Z] + D;
    }

    double getCoef(int ind) {
        switch (ind)
        {
        case 0:
            return A;
        case 1:
            return B;
        case 2:
            return C;
        case 3:
            return D;
        default:
            throw std::invalid_argument("Недопустимый индекс коэффициента");
        }
    }

    Surface() {
        A = 0, B = 0, C = 0, D = 0;
    }

    Surface(std::vector<Point> points)
    {
        if (nullptr == points.data()) {
            throw std::invalid_argument("Недопустимые аргументы конструтора");
        }

        this->getCoefs(points);
    }
};

class Tetrahedron {
private:
    //Вершины тетраэдера
    Point * points = new Point[VERTICESCOUNT];
    //Плоскости, в которых лежат грани тетраэдера
    Surface * surfaces[FACESCOUNT];

public:
    Tetrahedron(Point * points) : points(points)
    {
        if (nullptr == points) {
            throw std::invalid_argument("Недопустимые аргументы конструтора");
        }
        // Плоскости тетраэдера
        surfaces[0] = new Surface({ points[1], points[2], points[3] }); // Не содержит points[0]
        surfaces[1] = new Surface({ points[2], points[3], points[0] }); // Не содержит points[1]
        surfaces[2] = new Surface({ points[0], points[1], points[3] }); // Не содержит points[2]
        surfaces[3] = new Surface({ points[0], points[1], points[2] }); // Не содержит points[3]
    }

    bool isInside(Point point) {
        if (nullptr == point.coords) {
            throw std::invalid_argument("Точка непроинициализирована");
        }

        if (this->surfaces[0]->getValue(this->points[0]) * this->surfaces[0]->getValue(point) < 0)
        {
            return false;
        }
        else if (this->surfaces[1]->getValue(this->points[1]) * this->surfaces[1]->getValue(point) < 0) {
            return false;
        }
        else if (this->surfaces[2]->getValue(this->points[2]) * this->surfaces[2]->getValue(point) < 0) {
            return false;
        }
        else if (this->surfaces[3]->getValue(this->points[3]) * this->surfaces[3]->getValue(point) < 0) {
            return false;
        }
        
        return true;
    }

    double getLength(Point a, Point b) {
        if (nullptr == a.coords || nullptr == b.coords) {
            throw std::invalid_argument("Одна из точек непроинициализирована");
        }

        return sqrt(pow((a.coords[X] - b.coords[X]),2) +
                    pow((a.coords[Y]-b.coords[Y]),2) + 
                    pow((a.coords[Z] - b.coords[Z]),2));
    }

    double getSquare(int surfaceInd) {
        if (surfaceInd > FACESCOUNT-1) {
            throw std::invalid_argument("Недопустимый индекс грани");
        }

        std::vector<Point> surfaceVertices;
        for (int i = 0; i < 4; i++) {
            if (i != surfaceInd) {
                surfaceVertices.push_back(this->points[i]);
            }
        }

        double l1 = getLength(surfaceVertices.at(0), surfaceVertices.at(1));
        double l2 = getLength(surfaceVertices.at(1), surfaceVertices.at(2));
        double l3 = getLength(surfaceVertices.at(0), surfaceVertices.at(2));

        double halfPerimetr = (l1 + l2 + l3)/2;
        return sqrt(halfPerimetr * (halfPerimetr - l1) * (halfPerimetr - l2) * (halfPerimetr - l3));
    }

    double getMaxPointCoord(int coordInd) {
        if (coordInd > Z) {
            throw std::invalid_argument("Недопустимый индекс координаты пространства");
        }

        double s0 = getSquare(0);
        double s1 = getSquare(1);
        double s2 = getSquare(2);
        double s3 = getSquare(3);
        
        return (s0 * points[0].coords[coordInd] +
            s1 * points[1].coords[coordInd] +
            s2 * points[2].coords[coordInd] +
            s3 * points[3].coords[coordInd]) / (s0 + s1 + s2 + s3);
    }

    Point maxPoint() {
        double x = getMaxPointCoord(X);
        double y = getMaxPointCoord(Y);
        double z = getMaxPointCoord(Z);

        return Point({ x,y,z });
    }

    void print() {
        std::cout << "Тетраэдр с точками:" << std::endl;
        for (int i = 0; i < 4; i++) {
            std::cout << "(" << points[i].coords[X] << "," << points[i].coords[Y] << "," << points[i].coords[Z] << ")" << std::endl;
        }
    }

    ~Tetrahedron() {
        delete[] points;
        delete[] surfaces;
    }
};

int main()
{
    Point checkPoint({ 1, 1, 1 }); // Лежит
    //Point checkPoint({ 10, 10, 10 }); // Не лежит

    Point* points[VERTICESCOUNT];

    /*
    Point p1({3,-10,4});
    Point p2({ 9,9,4 });
    Point p3({5,6,-1});
    Point p4({ 2,6,3 });*/

    Point p1({ 0,0,0 });
    Point p2({ 5,0,0 });
    Point p3({ 0,5,0 });
    Point p4({ 0,0,5 });

    points[0] = &p1;
    points[1] = &p2;
    points[2] = &p3;
    points[3] = &p4;

    Tetrahedron tet(*points);
    tet.print();

    std::cout << "Проверяемая точка: (" << checkPoint.coords[X] << "," << checkPoint.coords[Y] << "," << checkPoint.coords[Z] << ")" << std::endl;

    if (tet.isInside(checkPoint)) {
        std::cout << "Точка принадлежит тетраэдору." << std::endl;
    }
    else {
        std::cout << "Точка не принадлежит тетраэдору" << std::endl;
    }

    Point max = tet.maxPoint();

    std::cout << "Координаты максильно удалённой от поверности тетраэдора точки:" << std::endl;
    std::cout << "x = " << max.coords[X] << std::endl;
    std::cout << "y = " << max.coords[Y] << std::endl;
    std::cout << "z = " << max.coords[Z] << std::endl;

    return 0;
}
